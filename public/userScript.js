$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // Ajax: admin/manage/account/index ==================================
    $(".btnDestroyAccount").click(function() {
        var url = $(this).data("url");
        var idAccount = $(this).val();
        // alert(idAccount);
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                id: idAccount
            },
            dataType: 'html',
            success: function(result) {
                if (result == 0) {
                    $("#account" + idAccount).fadeOut('slow', function(){
                        $("#account" + idAccount).remove();
                    });
                } else {
                    alert("Không thành công");
                }
            }
        });
    });

    // Ajax: admin/manage/category/index ==================================
    $(".btnDestroyCategory").click(function() {
        var url = $(this).data("url");
        var idCategory = $(this).val();
        // alert(idCategory);
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                id: idCategory
            },
            dataType: 'html',
            success: function(result) {
                if (result == 0) {
                    $("#category" + idCategory).fadeOut('slow', function(){
                        $("#category" + idCategory).remove();
                    });
                } else {
                    alert("Không thành công");
                }
            }
        });
    });

    // Ajax: admin/manage/publisher/index ==================================
    $(".btnDestroyPublishCompany").click(function() {
        var url = $(this).data("url");
        var idPub = $(this).val();
        // alert(idPub);
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                id: idPub
            },
            dataType: 'html',
            success: function(result) {
                if (result == 0) {
                    $("#publish" + idPub).fadeOut('slow', function(){
                        $("#publish" + idPub).remove();
                    });
                } else {
                    alert("Không thành công");
                }
            }
        });
    });

    // Ajax: admin/manage/book/show ==================================
    $(".btnDestroyBook").click(function() {
        var url = $(this).data("url");
        var idBook = $(this).val();
        // alert($(this).val());
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                id: idBook
            },
            dataType: 'html',
            success: function(result) {
                if (result == 0) {
                    $("#book" + idBook).fadeOut('slow', function(){
                        $("#book" + idBook).remove();
                    });
                } else {
                    alert("Không thành công");
                }
            }
        });
    });

    $(".btnDestroyVersion").click(function() {
        var url = $(this).data("url");
        var idPubBoo = $(this).val();
        // alert(idPubBoo);
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                id: idPubBoo
            },
            dataType: 'html',
            success: function(result) {
                if (result == 0) {
                    $("#version" + idPubBoo).fadeOut('slow', function(){
                        $("#version" + idPubBoo).remove();
                    });
                } else {
                    alert("Không thành công");
                }
            }
        });
    });

    // Ajax: admin/manage/book/child/show =============================
    $("#btnAdd").click(function() {
        var url = $(this).data("url");
        var idPubBoo = $("#idPubBoo").val();
        var no = $("#serial_no").val();
        var sta = $("#status").val();
        // alert(url);
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                serial_no: no,
                publish_company_book_id: idPubBoo,
                status: sta
            },
            dataType: 'html',
            success: function(result) {
                // alert(result);
                if (result == 0) {
                    //---------------------------------
                    $("#example > tbody").prepend("<tr id='row" + no + "'><td>" + no + "</td><td><select id='" + no + "' class='selectStatus form-control custom-select' name='statusTable'><option " + ((sta == 0) ? "selected" : "") + " value='0'>Chờ</option><option " + ((sta == 1) ? "selected" : "") + " value='1'>Mượn</option><option " + ((sta == 2) ? "selected" : "") + " value='2'>Mất</option><option " + ((sta == 3) ? "selected" : "") + " value='3'>Không được mượn</option></select></td><td><button value='" + no + "' id='btnSave" + no + "' data-url='" + $("#urlEdit").val() + "' type='button' class='btnSave btn btn-block btn-outline-info btn-lg' disabled>Lưu thay đổi</button></td><td><button value='" + no + "' data-url='" + $("#urlDestroy").val() + "' onclick='return alert('Bạn có chắc muốn xóa?')' type='button' class='btnDestroy btn btn-block btn-outline-danger btn-lg'>Xóa</button></td></tr>");
                    //---------------------------------
                } else if (result == 1) {
                    alert("Đã tồn tại.")
                } else {
                    alert("Mã số sai hoặc bỏ trống.");
                }

                $("#serial_no").val("");
                $("#status").val("0");
            }
        });
    });

    $(document).on("change", ".selectStatus", function () {
        var id = $(this).attr('id');
        $("#btnSave" + id).prop("disabled", false);
    });

    $(document).on("click", ".btnSave", function () {
        var url = $(this).data("url");
        var no = $(this).val();
        var sta = $("#" + no).val();
        // alert(sta);
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                serial_no: no,
                status: sta
            },
            dataType: 'html',
            success: function(result) {
                if (result == 0) {
                    $("#btnSave" + no).prop("disabled", true);
                } else {
                    alert("Không thành công");
                }
            }
        });
    });

    $(document).on("click", ".btnDestroy", function () {
        var url = $(this).data("url");
        var no = $(this).val();
        // alert(no);
        $.ajax({
            url: url,
            method: 'POST',
            data: {
                serial_no: no
            },
            dataType: 'html',
            success: function(result) {
                if (result == 0) {
                    $("#row" + no).fadeOut('slow', function(){
                        $("#row" + no).remove();
                    });
                } else {
                    alert("Không thành công");
                }
            }
        });
    });
})
