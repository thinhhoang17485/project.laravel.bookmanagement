<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Auth;

class StatisticalController extends Controller
{
    private function getHistoryBookData()
    {
        $data = DB::table('history_loan')
            ->join('book', 'book.id', '=', 'history_loan.serial_no_id')
            ->join('publish_company_book', 'publish_company_book.id', '=', 'book.publish_company_book_id')
            ->join('details_book', 'details_book.id', '=', 'publish_company_book.details_book_id')
            ->join('author_book', 'author_book.details_book_id', '=', 'details_book.id')
            ->join('details_author', 'details_author.id', '=', 'author_book.author_id')
            ->select(
                'history_loan.date_begin as date_begin',
                'history_loan.date_end as date_end',
                'history_loan.info as info',
                'history_loan.price as price',
                'book.serial_no as serial_no',
                'details_book.name as nameBook',
                'details_author.name as nameAuthor',
            )
            ->get();
        return $data;
    }
    private function getHistoryBookNoDateEndData()
    {
        $data = DB::table('history_loan')
            ->join('book', 'book.id', '=', 'history_loan.serial_no_id')
            ->join('publish_company_book', 'publish_company_book.id', '=', 'book.publish_company_book_id')
            ->join('details_book', 'details_book.id', '=', 'publish_company_book.details_book_id')
            ->join('author_book', 'author_book.details_book_id', '=', 'details_book.id')
            ->join('details_author', 'details_author.id', '=', 'author_book.author_id')
            ->where('history_loan.date_end', null)
            ->select(
                'history_loan.date_begin as date_begin',
                'history_loan.info as info',
                'history_loan.price as price',
                'book.serial_no as serial_no',
                'details_book.name as nameBook',
                'details_author.name as nameAuthor',
            )
            ->get();
        return $data;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all = $this->getHistoryBookData();
        $noDateEnd = $this->getHistoryBookNoDateEndData();
        return view('admin/modules/others/statistical/index')
            ->with([
                'all' => $all,
                'noDateEnd' => $noDateEnd
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
