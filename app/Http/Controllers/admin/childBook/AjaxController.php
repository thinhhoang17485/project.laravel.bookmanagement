<?php

namespace App\Http\Controllers\admin\childBook;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class AjaxController extends Controller
{
    public function addBook(Request $request)
    {
        $book['serial_no'] = $request->serial_no;
        $book['publish_company_book_id'] = $request->publish_company_book_id;
        $book['status'] = $request->status;
        $book['created_at'] = new DateTime;
        $book['updated_at'] = new DateTime;
        try {
            $isExist = DB::table('book')
                // ->where('publish_company_book_id', $request->publish_company_book_id)
                ->where('serial_no', $book['serial_no'])->get();

            if (sizeof($isExist) != 0) {
                return 1;
            }

            DB::table('book')->insert($book);
            return 0;
        } catch(\Exception $e) {
            return 2;
        }
    }

    public function editBook(Request $request)
    {
        try {
            DB::table('book')
                ->where('serial_no', $request->serial_no)
                ->update(['status' => $request->status]);
            return 0;
        } catch(\Exception $e) {
            return 1;
        }
    }

    public function destroyBook(Request $request)
    {
        try {
            DB::table('book')
                ->where('serial_no', $request->serial_no)
                ->delete();
            return 0;
        } catch(\Exception $e) {
            return 1;
        }
    }
}
