<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DateTime;

class WorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user/modules/management/work/index');
    }

    public function borrow()
    {
        return view('user/modules/management/work/borrow');
    }

    public function return()
    {
        return view('user/modules/management/work/return');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = $request->validate([
            'serial' => 'required|regex:/^[0-9]+$/',
            'price' => 'required|regex:/^[0-9]+$/',
        ],[
            'serial.required' => 'Bắt buộc.',
            'serial.regex' => 'Sai hình thức.',
            'price.required' => 'Bắt buộc.',
            'price.regex' => 'Sai hình thức.',
        ]);
        $raw = $request->except('_token');
        $idBook = DB::table('book')
            ->where('serial_no', $raw['serial'])
            // ->where('status', 0)
            ->value('id');
        if ($idBook == null) {
            return redirect()->back()->withErrors(['notfound' => 'Không tìm thấy!']);
        }

        $obLoan = DB::table('history_loan')
            ->where('serial_no_id', $idBook)
            ->orderByDesc('id')
            ->first();


        if ($obLoan == null) {
            return redirect()->back()->withErrors(['notfound' => 'Không tìm thấy']);
        }

        $isExist = ($obLoan->date_end == null);

        if ($isExist) {
            return redirect()->back()->withErrors(['notfound' => 'Trùng lặp!!!!']);
        }

        $data['serial_no_id'] = $idBook;
        $data['price'] = $raw['price'];
        $data['info'] = $raw['info'];
        $data['date_begin'] = new DateTime;
        $data['created_at'] = new DateTime;
        $data['updated_at'] = new DateTime;

        DB::table('history_loan')->insert($data);
        DB::table('book')
            ->where('id', $idBook)
            ->update(['status' => 1]);
        return view('user/modules/management/work/index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $validateData = $request->validate([
            'serial' => 'required|regex:/^[0-9]+$/',
        ],[
            'serial.required' => 'Bắt buộc.',
            'serial.regex' => 'Sai hình thức.',
        ]);
        $raw = $request->except('_token');
        $idBook = DB::table('book')
            ->where('serial_no', $raw['serial'])
            // ->where('status', 0)
            ->value('id');
        if ($idBook == null) {
            return redirect()->back()->withErrors(['notfound' => 'Không tìm thấy!']);
        }

        $obLoan = DB::table('history_loan')
            ->where('serial_no_id', $idBook)
            ->orderByDesc('id')
            ->first();
        $isExist = $obLoan->date_end == null;
        if (!$isExist) {
            return redirect()->back()->withErrors(['notfound' => 'Trùng lặp!!!!']);
        }
        // DB::transaction(function () {
            DB::table('history_loan')
                ->where('id', $obLoan->id)
                ->update(['date_end' => new DateTime]);
            DB::table('book')
                ->where('id', $idBook)
                ->update(['status' => 0]);
        // });
        return view('user/modules/management/work/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
