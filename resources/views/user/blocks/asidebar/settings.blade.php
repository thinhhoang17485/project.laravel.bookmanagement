<li class="nav-header">Thiết lập</li>
{{-- <li class="nav-item">
<a href="" class="nav-link @yield('settingButtonSelected', '')">
    <i class="nav-icon far fa-circle text-info"></i>
    <p>Cài đặt</p>
</a>
</li> --}}
<li class="nav-item">
<a href="{{ route('auth.logout') }}" class="nav-link" onclick="return alert('Bạn có chắc muốn đăng xuất không?');">
    <i class="nav-icon far fa-circle text-danger"></i>
    <p class="text">Đăng xuất</p>
</a>
</li>
