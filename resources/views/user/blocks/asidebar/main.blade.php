 <li class="nav-item">
    <a href="{{ route('user.manage.work.index') }}" class="nav-link @yield('workButtonSelected', '')">
       <i class="nav-icon fas fa-copy"></i>
       <p>
          Thao tác
       </p>
    </a>
 </li>
 <li class="nav-item">
    <a href="{{ route('user.manage.profile.index') }}" class="nav-link @yield('profileButtonSelected', '')">
       <i class="nav-icon fas fa-book"></i>
       <p>
          Hồ sơ
       </p>
    </a>
 </li>
