
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    @include('user/blocks/navbar/leftNavbar')
    <!-- SEARCH FORM -->
    @include('user/blocks/navbar/search')
    <!-- Right navbar links -->
    @include('user/blocks/navbar/rightNavbar')
 </nav>

