<!DOCTYPE html>
<head>
    @include('user/blocks/head')
</head>
<html>
   <body class="hold-transition sidebar-mini sidebar-collapse">
      <!-- Site wrapper -->
      <div class="wrapper">
         <!-- Navbar -->
         @include('user/blocks/navbar')
         <!-- Main Sidebar Container -->
         @include('user/blocks/asidebar')
         <!-- Content Wrapper. Contains page content -->
         @include('user/blocks/content')
         <!-- /.content-wrapper -->
         @include('user/blocks/footer')
      </div>
      <!-- ./wrapper -->
   </body>
   @include('user/blocks/foot')
</html>
