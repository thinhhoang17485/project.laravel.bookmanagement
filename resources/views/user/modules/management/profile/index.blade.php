@extends('user/master')
@section('title', 'Hồ sơ')
@section('taskname', 'Hiển thị')
@section('profileButtonSelected', 'active')

@section('content')
<!-- Main content -->
<section class="content">
    {{-- ====================================================== --}}
    <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Nhiệm vụ</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">

                <div class="container-fluid col-12">
                    <div class="row">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Tên thành viên</th>
                                        <th>Nhiệm vụ</th>
                                        <th>Tỉ lệ hoàn thành (%)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- =========================== --}}
                                    <tr>
                                        <td><b>Hoàng Ngọc Thịnh</b></td>
                                        <td>Thiết kế database.</td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Vẽ luồng, viết sườn, master layout cho dự án.</td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Nhúng giao diện admin (phần cá nhân).</td>
                                        <td>60%</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Chức năng:<br>
                                            - Thêm, xóa, sửa sách.<br>
                                            - Thêm, xóa, sửa các phiên bản sách.<br>
                                            - Thêm, xóa, sửa từng quyển sách.
                                        </td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Chỉnh sửa, thêm Ajax cho một số tính năng trên nền có sẵn.</td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Viết middleware kiểm tra Authentication.</td>
                                        <td>100%</td>
                                    </tr>
                                    {{-- =========================== --}}
                                    <tr>
                                        <td><b>Võ Minh Dương</b></td>
                                        <td>Chuyển sơ đồ database thành code.</td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Nhúng giao diện admin (phần cá nhân).</td>
                                        <td>40%</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Chức năng:<br>
                                            - Thêm, xóa, sửa tài khoản.<br>
                                            - Thêm, xóa, sửa nhà xuất bản.<br>
                                            - Thêm, xóa, sửa Thể loại.<br>
                                            - Thống kê.
                                        </td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Đẩy lên host.</td>
                                        <td>100%</td>
                                    </tr>
                                    {{-- =========================== --}}
                                    <tr>
                                        <td><b>Phan Gia Nhựt</b></td>
                                        <td>Nhúng giao diện Authentication.</td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Nhúng giao diện user.</td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Chức năng:<br>
                                            - Đăng kí, đăng nhập tài khoản.<br>
                                            - Mượn, trả sách.<br>
                                        </td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Viết middleware kiểm tra, điều hướng người dùng.</td>
                                        <td>100%</td>
                                    </tr>
                                    {{-- =========================== --}}
                                    <tr>
                                        <td><b>Nhâm Sỹ Nam</b></td>
                                        <td>Hỗ trợ nhúng giao diện.</td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Nhập liệu dữ liệu thô.</td>
                                        <td>100%</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>Báo cáo.</td>
                                        <td>100%</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Tên thành viên</th>
                                        <th>Nhiệm vụ</th>
                                        <th>Tỉ lệ hoàn thành (%)</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
    </div>
    {{-- ====================================================== --}}
    <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Timeline rút gọn</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">

                <div class="container-fluid col-12">
                    <div class="row">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Tuần: 1</th>
                                        <th>2</th>
                                        <th>3</th>
                                        <th>4, 5, 6, 7</th>
                                        <th>8, 9</th>
                                        <th>10</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>- Thiết kế database.<br>
                                            - Vẽ luồng.<br>
                                        </td>
                                        <td>
                                            - Viết sườn.<br>
                                            - Nhúng giao diện (40%).<br>
                                            - Chuyển đổi mô hình ERD thành code.
                                        </td>
                                        <td>
                                            - Nhúng giao diện (60% còn lại).<br>
                                            - Viết route.<br>
                                            - Viết middleware.
                                        </td>
                                        <td>
                                            - Viết các tính năng chính.
                                        </td>
                                        <td>
                                            - Hoàn thiện, thêm Ajax.
                                        </td>
                                        <td>
                                            - Test chéo, kiểm tra lỗi.<br>
                                            - Đẩy lên host.
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
    </div>
</section>
<!-- /.content -->
@endsection
