@extends('user/master')
@section('title', 'Thao tác')
@section('taskname', 'Chọn')
@section('workButtonSelected', 'active')

@section('content')
<!-- Main content -->
<section class="content">
    <div class="row">
      <div class="col-md-6">
        <div class="card card-primary">
            <div class="card-header"><h3 class="card-title">Mượn sách</h3></div>
            <div class="card-body">
                <p>Làm thủ tục mượn sách.</p>
                <a href="{{ route('user.manage.work.borrow') }}"><button class="btn btn-success float-right" type="submit">Thực hiện</button></a>
            </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="card card-primary">
            <div class="card-header"><h3 class="card-title">Trả sách</h3></div>
            <div class="card-body">
                <p>Làm thủ tục trả sách.</p>
                <a href="{{ route('user.manage.work.return') }}"><button class="btn btn-success float-right" type="submit">Thực hiện</button></a>
            </div>
        </div>
      </div>
    </div>
</section>
<!-- /.content -->
@endsection
