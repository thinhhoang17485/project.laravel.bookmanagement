@extends('user/master')
@section('title', 'Thao tác')
@section('taskname', 'Thủ tục Trả')
@section('workButtonSelected', 'active')

@section('content')
<!-- Main content -->
<section class="content">
    <form method="POST" action="{{ route('user.manage.work.update') }}">
    @csrf
    <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Điền thông tin</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="form-group">
                <label>Mã số sách
                    <span>
                        @if ($errors->has('serial'))
                        <strong style="color: red; font-size: 80%">
                            {{ $errors->first('serial') }}
                        </strong>
                        @endif
                    </span>
                    <span>
                        @if ($errors->has('notfound'))
                        <strong style="color: red; font-size: 80%">
                            {{ $errors->first('notfound') }}
                        </strong>
                        @endif
                    </span>
                </label>
                <input type="text" value="{{ old('serial') }}" class="form-control" name="serial">
            </div>
          </div>
          <!-- /.card-body -->
        </div>
        <div class="col-md-3"></div>
        <!-- /.card -->
      </div>
    </div>
    <div class="row">
    <div class="col-3"></div>
      <div class="col-6">
        <a href="{{ route('user.manage.work.index') }}" class="btn btn-secondary">Hủy</a>
        <button class="btn btn-success float-right" type="submit">Thực hiện</button>
      </div>
      <div class="col-3"></div>
    </div>
    </form>
</section>
<!-- /.content -->
@endsection
