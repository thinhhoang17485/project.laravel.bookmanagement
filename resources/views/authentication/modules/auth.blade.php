@extends('authentication/master')
@section('title', 'Đăng nhập - Đăng kí')

@section('content')
@if (!$errors->any() || $errors->has('emailL') || $errors->has('passwordL') || $errors->has('alert'))
    <input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Đăng nhập</label>
    <input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab">Đăng kí</label>
@else
    <input id="tab-1" type="radio" name="tab" class="sign-in"><label for="tab-1" class="tab">Đăng nhập</label>
    <input id="tab-2" type="radio" name="tab" class="sign-up" checked><label for="tab-2" class="tab">Đăng kí</label>
@endif
<div class="login-form">
    <div id="login" class="col s12 sign-in-htm">
        <form class="col s12" method="POST" action="{{ route('auth.progressLogin') }}">
            @csrf
            <div class="form-container">
                <h4 class="teal-text">Xin chào</h4>
                @error('emailL')
                    <strong>{{ $message }}</strong>
                @enderror
                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" type="email" class="validate text-color" name="emailL" value="{{ old('emailL') }}">
                        <label for="email">Email</label>
                    </div>
                </div>
                @error('passwordL')
                    <strong>{{ $message }}</strong>
                @enderror
                <div class="row">
                    <div class="input-field col s12">
                        <input id="password" type="password" class="validate text-color" name="passwordL">
                        <label for="password">Mật khẩu</label>
                    </div>
                </div>
                @error('alert')
                    <div class="input-field col s12">
                        <strong>{{ $message }}</strong>
                    </div>
                @enderror
                <br>
                <center>
                    <button class="btn waves-effect waves-light teal" type="submit">Đăng nhập</button>
                    <br>
                    <br>
                    <a href="">Quên mật khẩu?</a>
                </center>
            </div>
        </form>
    </div>
    <div id="register" class="col s12 sign-up-htm">
        <form class="col s12" method="POST" action="{{ route('auth.progressRegister') }}">
            @csrf
            <div class="form-container">
                <h5 class="teal-text">Thành viên mới</h5>
                @if ($errors->has('lastname') || $errors->has('firstname'))
                    <strong>Họ tên bắt buộc!</strong>
                @endif
                <div class="row">
                    <div class="input-field col s6">
                        <input id="last_name" type="text" class="validate text-color" name="lastname" value="{{ old('lastname') }}">
                        <label for="last_name">Họ</label>
                    </div>
                    <div class="input-field col s6">
                        <input id="last_name" type="text" class="validate text-color" name="firstname" value="{{ old('firstname') }}">
                        <label for="last_name">Tên</label>
                    </div>
                </div>
                @error('email')
                    <strong>{{ $message }}</strong>
                @enderror
                <div class="row">
                    <div class="input-field col s12">
                        <input id="email" type="email" class="validate text-color" name="email" value="{{ old('email') }}">
                        <label for="email">Email</label>
                    </div>
                </div>
                @error('password')
                    <strong>{{ $message }}</strong>
                @enderror
                <div class="row">
                    <div class="input-field col s12">
                        <input id="password" type="password" class="validate text-color" name="password">
                        <label for="password">Mật khẩu</label>
                    </div>
                </div>
                @error('password_confirmation')
                    <strong>{{ $message }}</strong>
                @enderror
                <div class="row input-field col s12">
                    <div class="input-field col s12">
                        <input id="password-confirm" type="password" class="validate text-color" name="password_confirmation">
                        <label for="password-confirm">Xác nhận mật khẩu</label>
                    </div>
                </div>
                <center>
                    <button class="btn waves-effect waves-light teal" type="submit">Đăng kí</button>
                </center>
            </div>
        </form>
    </div>
</div>
@endsection
