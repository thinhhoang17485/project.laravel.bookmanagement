<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>@yield('title', 'Authentication')</title>
<link rel="stylesheet" href="{{ asset('authentication/loginandregister/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('authentication/loginandregister/css/materialize.css') }}">
<link rel="stylesheet" href="{{ asset('authentication/loginandregister/css/materialize.min.css') }}">
