@extends('admin/master')
@section('title', 'Thể loại')
@section('taskname', 'Danh sách')
@section('managementButtonOpen', 'menu-open')
@section('managementButtonSelected', 'active')
@section('managementButtonCategorySelected', 'active')

@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid col-10">
        <div class="row">
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Tên</th>
                        <th>Mô tả</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($categories as $category)
                    <tr id="{{ "category" . $category->id }}">
                        <td>{{ $category->name }}</td>
                        <td>{{ $category->description }}</td>
                        <td>
                            <a href="{{ route('admin.manage.category.edit', ['id' => $category->id]) }}"><button type="button" class="btn btn-block btn-outline-secondary btn-lg">Sửa</button></a>
                        </td>
                        <td>
                            <button
                                value="{{ $category->id }}"
                                data-url="{{ route('admin.manage.category.destroy') }}"
                                onclick="return alert('Bạn có chắc muốn xóa không?');"
                                type="button" class="btnDestroyCategory btn btn-block btn-outline-danger btn-lg">
                                Xóa
                            </button>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Tên</th>
                        <th>Mô tả</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
