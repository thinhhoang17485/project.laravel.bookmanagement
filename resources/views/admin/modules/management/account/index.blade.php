@extends('admin/master')
@section('title', 'Tài khoản')
@section('taskname', 'Danh sách')
@section('managementButtonOpen', 'menu-open')
@section('managementButtonSelected', 'active')
@section('managementButtonAccountSelected', 'active')

@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Tên</th>
                        <th>Email</th>
                        <th>Phân quyền</th>
                        <th>Trạng thái</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr id="{{ "account" . $user->id }}">
                            <td>{{ $user->lastname . " " . $user->firstname }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ ($user->isAdmin) ? 'Admin' : 'Người dùng'}}</td>
                            <td>{{ ($user->active) ? 'Kích hoạt' : 'Vô hiệu'}}</td>
                            <td>
                                <a href="{{ route('admin.manage.account.edit', ['id' => $user->id]) }}"><button type="button" class="btn btn-block btn-outline-secondary btn-lg" {{ ($user->email == $currentEmail) ? 'disabled' : '' }}>Cấu hình</button></a>
                            </td>
                            <td>
                                <button
                                    data-url="{{ route('admin.manage.account.destroy') }}"
                                    value="{{ $user->id }}"
                                    onclick="return alert('Bạn có chắc muốn xóa không?');"
                                    type="button"
                                    class="btnDestroyAccount btn btn-block btn-outline-danger btn-lg" {{ ($user->email == $currentEmail) ? 'disabled' : '' }}>
                                    Xóa
                                </button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Tên</th>
                        <th>Email</th>
                        <th>Phân quyền</th>
                        <th>Trạng thái</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
