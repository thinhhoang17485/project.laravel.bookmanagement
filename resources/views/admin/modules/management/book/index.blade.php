@extends('admin/master')
@section('title', 'Sách')
@section('taskname', 'Danh sách')
@section('managementButtonOpen', 'menu-open')
@section('managementButtonSelected', 'active')
@section('managementButtonBookSelected', 'active')

@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Tên sách</th>
                        <th>Tác giả</th>
                        <th>Mô tả</th>
                        <th>Ngày</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($data as $item)
                    <tr id="{{ "book" . $item->idBook }}">
                        <td>{{ $item->nameBook }}</td>
                        <td>{{ $item->nameAuthor }}</td>
                        <td>{{ $item->descriptionBook }}</td>
                        <td>{{ $item->dateBook }}</td>
                        <td>
                            <a href="{{ route('admin.manage.book.show', ['id' => $item->idBook]) }}">
                                <button type="button" class="btn btn-block btn-outline-info btn-lg">Chi tiết</button>
                            </a>
                        </td>
                        <td>
                            <button
                                value="{{ $item->idBook }}"
                                data-url="{{ route('admin.manage.book.destroyBookAjax') }}"
                                onclick="return alert('Bạn có chắc muốn xóa?')"
                                type="button"
                                class="btnDestroyBook btn btn-block btn-outline-danger btn-lg">
                                Xóa
                            </button>
                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Tên sách</th>
                        <th>Tác giả</th>
                        <th>Mô tả</th>
                        <th>Ngày</th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
