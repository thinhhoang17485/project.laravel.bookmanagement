@extends('admin/master')
@section('title', 'Sách')
@section('taskname', 'Tạo mới phiên bản khác từ sách có sẵn')
@section('additionButtonOpen', 'menu-open')
@section('additionButtonSelected', 'active')
@section('additionButtonBookSelected', 'active')
{{-- @section('username', '????') --}}

@section('content')
<!-- Main content -->
<section class="content">
    <div class="container-fluid col-10">
        <div class="row">
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Tên sách</th>
                        <th>Tác giả</th>
                        <th>Mô tả</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($data as $item)
                    <tr>
                        <td>{{ $item->nameBook }}</td>
                        <td>{{ $item->nameAuthor }}</td>
                        <td>{{ $item->descriptionBook }}</td>
                        <td>
                            <a href="{{ route('admin.manage.book.create.existing', ['id' => $item->idBook]) }}">
                                <button type="button" class="btn btn-block btn-outline-primary btn-lg">Chọn</button>
                            </a>
                        </td>
                    </tr>
                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Tên sách</th>
                        <th>Tác giả</th>
                        <th>Mô tả</th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
