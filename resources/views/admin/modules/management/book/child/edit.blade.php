@extends('admin/master')
@section('title', 'Sách')
@section('taskname', 'Sửa')
@section('managementButtonOpen', 'menu-open')
@section('managementButtonSelected', 'active')
@section('managementButtonBookSelected', 'active')

@section('content')
<!-- Main content -->
<section class="content">
    <form method="POST" action="{{ route('admin.manage.book.child.update', ['id' => $id]) }}">
    @csrf
    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Thông tin</h3>

            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i class="fas fa-minus"></i></button>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="inputName">Tên sách</label>
                    <input type="text" id="inputName" class="form-control" name="name" value="{{ $book->nameBook }}" disabled>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputName">Tác giả</label>
                    <input type="text" id="inputName" class="form-control" name="author" value="{{ $book->nameAuthor }}" disabled>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label>Thể loại</label>
                    <div class="select2-primary">
                    <select name="categories[]" class="select2" multiple data-placeholder="Chọn" data-dropdown-css-class="select2-primary" style="width: 100%;" disabled>
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}"  {{ (in_array($category->id, $categorySelected)) ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                    </select>
                    </div>
                </div>
                <div class="form-group col-md-6">
                    <label for="inputStatus">Nhà xuất bản</label>
                    <select class="form-control custom-select" name="publishCompany">
                    @foreach ($publish_company as $pub)
                        <option value="{{ $pub->id }}" {{ $pub->name == $payload->namePublishCompany ? 'selected' : ''}}>{{ $pub->name }}</option>
                    @endforeach
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6">
                    <label for="inputDescription">Mô tả</label>
                    <textarea id="inputDescription" class="form-control" rows="4" name="description" disabled>{{ $book->descriptionBook }}</textarea>
                    {{-- <script>CKEDITOR.replace('description')</script> --}}
                </div>

                <div class="form-group col-md-6">
                    <label for="inputDescription">Thông số</label>
                    <textarea id="inputDescription" class="form-control" rows="4" name="parameter">{{ $payload->parameterBook }}</textarea>
                    {{-- <script>CKEDITOR.replace('description')</script> --}}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-6"></div>
                <div class="form-group col-md-6">
                    <label for="inputName">Giá</label>
                    <input type="text" id="inputName" class="form-control" name="price" value="{{ $payload->priceBook }}">
                </div>
            </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

      </div>
      <div class="row">
        <div class="col-12">
          <a href="{{ route('admin.manage.book.child.show', ['id' => $payload->idPubBoo]) }}" class="btn btn-secondary">Hủy</a>
          <button class="btn btn-success float-right" type="submit">Thực hiện</button>
        </div>
      </div>
    </div>
    </form>
</section>
<!-- /.content -->
@endsection
