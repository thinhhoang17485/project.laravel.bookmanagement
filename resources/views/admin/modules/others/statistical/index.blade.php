@extends('admin/master')
@section('title', 'Thống kê')
@section('taskname', 'Hiển thị')
@section('statisticalButtonSelected', 'active')

@section('content')
<!-- Main content -->
<section class="content">
    {{-- ====================================================== --}}
    <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Tổng các sách được mượn</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">

                <div class="container-fluid col-12">
                    <div class="row">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Tên sách</th>
                                        <th>Tác giả</th>
                                        <th>Mã số</th>
                                        <th>Ngày mượn</th>
                                        <th>Ngày trả</th>
                                        <th>Thông tin</th>
                                        <th>Phí</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($all as $item)
                                    <tr>
                                        <td>{{ $item->nameBook }}</td>
                                        <td>{{ $item->nameAuthor }}</td>
                                        <td>{{ $item->serial_no }}</td>
                                        <td>{{ $item->date_begin }}</td>
                                        <td>{{ ($item->date_end == null) ? "-" : $item->date_end }}</td>
                                        <td>{{ $item->info }}</td>
                                        <td>{{ $item->price }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Tên sách</th>
                                        <th>Tác giả</th>
                                        <th>Mã số</th>
                                        <th>Ngày mượn</th>
                                        <th>Ngày trả</th>
                                        <th>Thông tin</th>
                                        <th>Phí</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
    </div>

    {{-- ====================================================== --}}
    <div class="row">
        <div class="col-md-12">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Sách chưa trả</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">

                <div class="container-fluid col-12">
                    <div class="row">
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>Tên sách</th>
                                    <th>Tác giả</th>
                                    <th>Mã số</th>
                                    <th>Ngày mượn</th>
                                    <th>Thông tin</th>
                                    <th>Phí</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($noDateEnd as $item)
                                <tr>
                                    <td>{{ $item->nameBook }}</td>
                                    <td>{{ $item->nameAuthor }}</td>
                                    <td>{{ $item->serial_no }}</td>
                                    <td>{{ $item->date_begin }}</td>
                                    <td>{{ $item->info }}</td>
                                    <td>{{ $item->price }}</td>
                                </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Tên sách</th>
                                    <th>Tác giả</th>
                                    <th>Mã số</th>
                                    <th>Ngày mượn</th>
                                    <th>Thông tin</th>
                                    <th>Phí</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
    </div>
</section>


<!-- /.content -->
@endsection
