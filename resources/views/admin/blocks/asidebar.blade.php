<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    @include('admin/blocks/asidebar/logo')
    <!-- Sidebar -->
    <div class="sidebar">
       <!-- Sidebar user (optional) -->
       @include('admin/blocks/asidebar/avatar')
       <!-- Sidebar Menu -->
       <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
             <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
             @include('admin/blocks/asidebar/main')
             @include('admin/blocks/asidebar/others')
             @include('admin/blocks/asidebar/settings')
          </ul>
       </nav>
       <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
 </aside>
