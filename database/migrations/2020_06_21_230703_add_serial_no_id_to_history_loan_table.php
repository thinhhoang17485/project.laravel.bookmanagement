<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSerialNoIdToHistoryLoanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('history_loan', function (Blueprint $table) {
            $table->unsignedBigInteger('serial_no_id')->after('id');

            $table->foreign('serial_no_id')->references('id')->on('book');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('history_loan', function (Blueprint $table) {
            //
        });
    }
}
