<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPublishCompanyIdToPublishCompanyBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('publish_company_book', function (Blueprint $table) {
            $table->unsignedBigInteger('publish_company_id')->after('id');

            $table->foreign('publish_company_id')->references('id')->on('publish_company');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('publish_company_book', function (Blueprint $table) {
            //
        });
    }
}
